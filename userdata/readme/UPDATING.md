TTU has a set schedule for updating the game. This is to prevent:
- Code mishaps. 
- Overwritten commits. 
- Sync issues. 
====
Schedule:

Monday - Friday: 
- Nick from 12 PM - 4 PM 
- John from 4 PM - 8 PM
- Frank/Grant (Tagteam) from 8 PM - 12 AM
----
Saturday - Sunday: 
- John from 8 AM - 12 PM 
- Nick from 12 PM - 4 PM
- Grant from 4 PM - 8 PM
- Frank from 8 PM - 12 AM
====
