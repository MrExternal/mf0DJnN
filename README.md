# Toontown Unknown #
====

Toontown Unknown is inspired from *Disney's Toontown Online*. It's *base source* is Toontown Rewritten. 

====

# About #

TTU started off on May 22, 2016. It was originally *founded by* Nickdoge, Chaos, King Dogg, Frank, and Mr. External. 
The game started off with Rewritten's source from 2014, and has since begun the transition to remove *all* of Rewritten's code, and replace it with Toontown Online's or our own. *Toontown Unknown is NOT a public server, nor even plans to be. It is a friends/invite only project that has no purpose of making revunue or being a regular game in the community.* 

*Credit is given to Rewritten, keep reading.* 

====

# Contributing #

- Please, ask to be added as a collaborator. *Do not fork the repository.* 
- Once you are accepted as a collaborator, you may *clone* the repository to your Git Desktop client. 
- After cloning, sync the repository *before anything else*, and wait for it to finish. 
- Once synced, you may make changes to the source as necessary. When you are done, *check your code and make sure it works.* 
- If your code works, commit it to Master and sync. If your code does NOT work, *fix it* and then commit. 

NOTES (READ): 

- *If you see PRC files in your "Changes", please discard them. They are worthless and will only make your commit take longer.* 
- *If your changes are submitted and they do not work, please revert your commit, otherwise you may be removed as a collaborator.*
- *Do NOT make a server out of this source, please. We do not want others using our work to make a server that will most likely fail.*

====

# Credits #

- Toontown Rewritten: *Base source code.* 
- Nickdoge & Chaos: *For their amazing commitment to the project.*
- Mr. External: *Server host and manager. Credit for being a minor developer and assisting with patch testing.* 
- King Dogg & Frank: *Also assisted with development.* 

====
