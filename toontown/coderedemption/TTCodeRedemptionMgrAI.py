from direct.directnotify import DirectNotifyGlobal
from direct.distributed.DistributedObjectAI import DistributedObjectAI
from toontown.toonbase import ToontownGlobals
from toontown.catalog import CatalogItem
from toontown.catalog.CatalogItemList import CatalogItemList
from toontown.catalog.CatalogPoleItem import CatalogPoleItem
from toontown.catalog.CatalogBeanItem import CatalogBeanItem
from toontown.catalog.CatalogChatItem import CatalogChatItem
from toontown.catalog.CatalogClothingItem import CatalogClothingItem, getAllClothes
from toontown.catalog.CatalogAccessoryItem import CatalogAccessoryItem
from toontown.catalog.CatalogRentalItem import CatalogRentalItem
from toontown.catalog.CatalogInvalidItem import CatalogInvalidItem
from toontown.catalog import CatalogGardenItem
from toontown.catalog import CatalogGardenStarterItem
import time

class TTCodeRedemptionMgrAI(DistributedObjectAI):
    notify = DirectNotifyGlobal.directNotify.newCategory("TTCodeRedemptionMgrAI")

    # Contexts
    Success = 0
    InvalidCode = 1
    ExpiredCode = 2
    Ineligible = 3
    AwardError = 4
    TooManyFails = 5
    ServiceUnavailable = 6

    def __init__(self, air): #Main file
        DistributedObjectAI.__init__(self, air)
        self.air = air

    def announceGenerate(self):
        DistributedObjectAI.announceGenerate(self)

    def delete(self):
        DistributedObjectAI.delete(self)

    def giveAwardToToonResult(self, todo0, todo1):
        pass

    def redeemCode(self, context, code):
        avId = self.air.getAvatarIdFromSender()
        if not avId:
            self.air.writeServerEvent('suspicious', avId=avId, issue='Tried to redeem a code from an invalid avId')
            return
		
        av = self.air.doId2do.get(avId)
        if not av:
            self.air.writeServerEvent('suspicious', avId=avId, issue='Invalid avatar tried to redeem a code')
            return

        # Some constants, which we need them
        valid = True
        eligible = True
        expired = False
        delivered = False

        # Get our redeemed codes
        codes = av.getRedeemedCodes()
        print codes
        if not codes:
            codes = [code]
            av.setRedeemedCodes(codes)
        else:
            if not code in codes:
                codes.append(code)
                av.setRedeemedCodes(codes)
                valid = True
            else:
                valid = False

        #Let's verify the code
        if not valid:
            self.air.writeServerEvent('code-redeemed', avId=avId, issue='Invalid code: %s' % code)
            self.sendUpdateToAvatarId(avId, 'redeemCodeResult', [context, self.InvalidCode, 0])
            return

        # Did our shit expire?
        if expired:
            self.air.writeServerEvent('code-redeemed', avId=avId, issue='Expired code: %s' % code)
            self.sendUpdateToAvatarId(avId, 'redeemCodeResult', [context, self.ExpiredCode, 0])
            return

        # Are we able to redeem this code?
        if not eligible:
            self.air.writeServerEvent('code-redeemed', avId=avId, issue='Ineligible for code: %s' % code)
            self.sendUpdateToAvatarId(avId, 'redeemCodeResult', [context, self.Ineligible, 0])
            return

        #Now let's Confirm the code.
        items = self.getItemsForCode(code) #Get the item
        for item in items:
            if isinstance(item, CatalogInvalidItem): # rlly nigger?
                self.air.writeServerEvent('suspicious', avId=avId, issue='Invalid CatalogItem\'s for code: %s' % code)
                self.sendUpdateToAvatarId(avId, 'redeemCodeResult', [context, self.InvalidCode, 0])
                break

            if len(av.mailboxContents) + len(av.onGiftOrder) >= ToontownGlobals.MaxMailboxContents:
                # Mailbox is full, No presents 4 you, fag!
                delivered = False
                break

            item.deliveryDate = int(time.time() / 60) + 1 # Let's just deliver the item right away.
            av.onOrder.append(item)
            av.b_setDeliverySchedule(av.onOrder)
            delivered = True

        if not delivered:
            # 0 is Success
            # 1, 2, 15, & 16 is an UnknownError
            # 3 & 4 is MailboxFull
            # 5 & 10 is AlreadyInMailbox
            # 6, 7, & 11 is AlreadyInQueue
            # 8 is AlreadyInCloset
            # 9 is AlreadyBeingWorn
            # 12, 13, & 14 is AlreadyReceived
            self.air.writeServerEvent('code-redeemed', avId=avId, issue='Could not deliver items for code: %s' % code)
            self.sendUpdateToAvatarId(avId, 'redeemCodeResult', [context, self.InvalidCode, 0])
            return

        # Send the item and tell the user its working
        self.air.writeServerEvent('code-redeemed', avId=avId, issue='Successfuly redeemed code: %s' % code)
        self.sendUpdateToAvatarId(avId, 'redeemCodeResult', [context, self.Success, 0])

    def getItemsForCode(self, code):
        avId = self.air.getAvatarIdFromSender()
        if not avId:
            self.air.writeServerEvent('suspicious', avId=avId, issue='Could not parse the gender of an invalid avId')
            return

        av = self.air.doId2do.get(avId)
        if not av:
            self.air.writeServerEvent('suspicious', avId=avId, issue='Could not parse the gender of an invalid avatar')
            return

        code = code.lower() # Anti-frustration features, activate!

	#I'm going to keep these codes, too lazy to add a time expire system
	#Here is how it works, Since we coded this part from scratch this will be different :(
	
        if code == "gadzooks": #The code input
            shirt = CatalogClothingItem(1807, 0) #The catalog item
            return [shirt]	#Return to the item
			
        if code == "weed":
            hat = CatalogAccessoryItem(102, 0)
            return [hat]
        
	if code == "crown" or code == "king":
            nigger = CatalogAccessoryItem(120, 0)
            return [nigger]
			
        if code == "superman" or code == "Supertoon":
			shirt = CatalogClothingItem(1124, 0)
			return[shirt]
			
        if code == "sillymeter" or code == "silly meter" or code == "silly-meter":
            shirt = CatalogClothingItem(1753, 0)
            return [shirt]

        if code == "gc-sbfo" or code == "gc sbfo" or code == "gcsbfo":
            shirt = CatalogClothingItem(1788, 0)
            return [shirt]

        if code == "getconnected" or code == "get connected" or code == "get_connected":
            shirt = CatalogClothingItem(1752, 0)#o dam
            return [shirt]

        if code == "summer":
            shirt = CatalogClothingItem(1709, 0)
            return [shirt]

        if code == "brrrgh":
            shirt = CatalogClothingItem(1800, 0)
            return [shirt]
			
        if code == "garden":
            weed = CatalogGardenStarterItem.CatalogGardenStarterItem()
            return [weed]

        if code == "toontastic":
            shirt = CatalogClothingItem(1820, 0)
            return [shirt]

        if code == "sunburst":
            shirt = CatalogClothingItem(1809, 0)
            return [shirt]

        if code == "sweet":
            beans = CatalogBeanItem(12000, tagCode = 2)
            return [beans]

        if code == "winter" or code == "cannons": #Cannons don't seem to work, but lets add it in for the future
            rent = CatalogRentalItem(ToontownGlobals.RentalCannon, 48*60, 0)
            return [rent]
		
        if code == "freecannon":
            launch = CatalogRentalItem(ToontownGlobals.RentalCannon, 420*60, 0)
            return [launch]
        return []

    def requestCodeRedeem(self, todo0, todo1):
        pass

    def redeemCodeResult(self, todo0, todo1, todo2):
        pass #TBD
